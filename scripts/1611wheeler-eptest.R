source("1611wheeler-errorpropfuncs.R")

# A function for calculating simple quad functional form data with defined noise terms
cSQfunc <- function(xv,int,Ia,xvSD,intSD,IaSD,nobs=1){
  xvn <- xv + rnorm(n=length(xv),sd=xvSD)
  xt <- xvn
  yt <- xv
  for(i in 1:length(yt)){yt[i] <- (int+rnorm(1,sd=intSD)) + ((Ia+rnorm(1,sd=IaSD))*(xt[i]^2)) }
  if(nobs>1){
    for(j in 2:nobs){
      xven <- xv + rnorm(n=length(xv),sd=xvSD)
      xt <- c(xt,xven)
      yte <- xv
      for(i in 1:length(yte)){yte[i] <- (int+rnorm(1,sd=intSD)) + ((Ia+rnorm(1,sd=IaSD))*(xven[i]^2)) }
      yt <- c(yt,yte)
    }
  }
  return(list(xt,yt))
}

# A function for calculating quad functional form data with defined noise terms
cQfunc <- function(xv,int,Ia,Ib,xvSD,intSD,IaSD,IbSD,nobs=1){
  xvn <- xv + rnorm(n=length(xv),sd=xvSD)
  xt <- xvn
  yt <- xv
  for(i in 1:length(yt)){yt[i] <- (int+rnorm(1,sd=intSD)) + ((Ia+rnorm(1,sd=IaSD))*(xvn[i]^1)) + ((Ib+rnorm(1,sd=IbSD))*(xvn[i]^2)) }
  if(nobs>1){
    for(j in 2:nobs){
      xven <- xv + rnorm(n=length(xv),sd=xvSD)
      xt <- c(xt,xven)
      yte <- xv
      for(i in 1:length(yte)){yte[i] <- (int+rnorm(1,sd=intSD)) + ((Ia+rnorm(1,sd=IaSD))*(xven[i]^1)) + ((Ib+rnorm(1,sd=IbSD))*(xven[i]^2)) }
      yt <- c(yt,yte)
    }
  }
  return(list(xt,yt))
}

# Define number of repeats
nreps <- 3
# Define number of x positions
nxp <- 10
# Define number of observations at each point
nobs <- 10

# Define the X terms
xmin <- 1
xmax <- 100
xvSD <- 1

# Define the Y terms
cint <- 5
cintSD <- 1000
cIa <- 2
cIaSD <- 0.5

# Plot window parameters
#x11(width=14,height=(2.8*2))
par(mfcol=c(2,nreps),mar=c(0,0,0,0),mgp=c(0,0,0),xaxt='n',yaxt='n')

# Repeat 5 times for 5 plots
for(i in 1:nreps){
# Calculate the X values
xv <- seq(xmin,xmax,length.out=nxp)

# Calculate the Y values (with noise)
SQres <- cSQfunc(xv,cint,cIa,xvSD,cintSD,cIaSD,nobs)
# Calculate the Y values (without noise)
SQresm <- cSQfunc(xv,cint,cIa,rep(0,nxp),rep(0,nxp),rep(0,nxp),nobs)

# Calculate the y noise SD using the error propagation method
yn <- 1:nxp; for(i in 1:nxp){ yn[i] <- epSQuad(cintSD,cIa,cIaSD,xv[i],xvSD) }

# Identify the ylim values
ymin <- min(SQresm[[2]]-(4*yn)); ymax <- max(SQresm[[2]]+(4*yn))

# Plot the points
plot(SQres[[1]],SQres[[2]],ylim=c(ymin,ymax))

# Add fit line (No noise), and StDev lines 1-3
inds <- seq(1,nxp,1)
lines(SQresm[[1]][inds],SQresm[[2]][inds])
lines(SQresm[[1]][inds],SQresm[[2]][inds]+yn,col='green'); lines(SQresm[[1]][inds],SQresm[[2]][inds]-yn,col='green')
lines(SQresm[[1]][inds],SQresm[[2]][inds]+(2*yn),col='blue'); lines(SQresm[[1]][inds],SQresm[[2]][inds]-(2*yn),col='blue')
lines(SQresm[[1]][inds],SQresm[[2]][inds]+(3*yn),col='red'); lines(SQresm[[1]][inds],SQresm[[2]][inds]-(3*yn),col='red')

# Calculate post-hoc stat power
pv <- 2:nxp
for(i in 2:nxp){
  m1 <- SQresm[[2]][[i-1]]
  m2 <- SQresm[[2]][[i]]
  pv[i-1] <- calcpow(nobs,m1,m2,yn[i-1],yn[i])
}

pv <- c(NA,pv)

plot(xv,pv,type='l',ylim=c(0,1))
abline(h=c(0,1),lwd=2)

}


